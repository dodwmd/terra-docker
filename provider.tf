# Configure the Docker provider
provider "docker" {
    host = "tcp://${var.docker_host}/"
    ca_material = "${path.module}/certs/ca.pem"
    cert_material = "${path.module}/certs/cert.pem"
    key_material = "${path.module}/certs/key.pem"
}
