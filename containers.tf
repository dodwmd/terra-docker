# create registry container
resource "docker_container" "registry" {
  image = "${docker_image.registry.latest}"
  name = "registry"
  hostname = "registry"
  env = ["REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt", "REGISTRY_HTTP_TLS_KEY=/certs/domain-key.crt"]
  must_run="true"
  ports {
    internal = 5000
  }
  volumes {
    container_path  = "/certs"
    host_path = "/certs/registry"
    read_only = true
  }
}

# create nginx container
resource "docker_container" "nginx" {
  image = "${docker_image.nginx.latest}"
  name = "nginx"
  hostname = "nginx"
  must_run="true"
  ports {
    internal = 8080
  }
}

#create bamboo container
resource "docker_container" "bamboo" {
  image = "${docker_image.bamboo.latest}"
  name = "bamboo"
  hostname = "bamboo"
  must_run="true"
  ports {
    internal = 8085
  }
}

