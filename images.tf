resource "docker_image" "registry" {
  name = "registry:2"
}

resource "docker_image" "nginx" {
  name = "dodwmd/nginx"
}

resource "docker_image" "bamboo" {
  name = "dodwmd/bamboo"
}
